smallc_program ::= type_specifier id ‘(‘ param_decl_list ‘)’ compound_stmt
type_specifier ::= int | char
param_decl_list ::= parameter_decl (‘,’ parameter_decl )*
param_decl ::= type_specifier id
compound_stmt ::= ‘{‘ (var_decl* stmt* expr_stmt* )? ‘}’
var_decl ::= type_specifier var_decl_list ‘;’
var_decl_list ::= variable_id ( ‘,’ variable_id)*
variable_id ::= id ( ‘=’ expr )?
stmt ::= compound_stmt | cond_stmt | while_stmt | break ‘;’ | continue ‘;’ | return expr ‘;’ | readint ‘(‘ id ‘)’ ‘;’ | writeint ‘(‘ expr ‘)’ ‘;’
cond_stmt ::= if ‘(‘ expr ‘)’ stmt (else stmt)?
while_stmt ::= while ‘(‘ expr ‘)’ stmt
expr_stmt ::= id '=' expr ';'
expr ::= id ‘=’ expr | condition
condition ::= disjunction | disjunction ‘?’ expr ‘:’ condition
disjunction ::= conjunction | disjunction ‘||’ conjunction
conjunction ::= comparison | conjunction ‘&&’ comparison
comparison ::= relation | relation ‘==’ relation
relation ::= sum | sum (‘<’ | ‘>’) sum
sum ::= sum ‘+’ term | sum ‘-’ term | term
term ::= term ‘*’ factor | term ‘/’ factor | term ‘%’ factor | factor
factor ::= ‘!’ factor | ‘-’ factor | primary
primary ::= num | charconst | id | ‘(‘ expr ‘)’
