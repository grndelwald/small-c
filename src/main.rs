use std::collections::VecDeque;
use std::convert::From;
use std::io::Write;

#[derive(Debug)]
pub struct Buffer{
    buffer: VecDeque<char>,
}

impl From<VecDeque<char>> for Buffer{
    fn from(f: VecDeque<char>) -> Self {
        Buffer{
            buffer: f,
        }
    }
}

#[derive(PartialEq,Debug,Clone)]
pub enum Token{
    Eof,
    Ascii(String),
    Integer(i32),
    Identifier(String),
    StringLiteral(String),
    Float(f32),
    Character(char),
    While,
    If,
    Else,
    Break,
    Continue,
    Return,
    ReadInt,
    WriteInt,
    Int,
    Char
}

trait BufferTraits {
    fn getchar(&mut self) -> char;
    fn push_front(&mut self,char_: char);
}

impl BufferTraits for Buffer {
    fn getchar(&mut self) -> char {
        match self.buffer.pop_front() {
            Some(x) => x,
            None => panic!("Reached Eof"),
        }
    }

    fn push_front(&mut self,char_: char) {
        self.buffer.push_front(char_);
    }
}

struct TempBuffer {
    buffer: Buffer,
    temp: VecDeque<Token>,
}

impl From<Buffer> for TempBuffer {
    fn from(f: Buffer) -> Self {
        TempBuffer{
            buffer: f,
            temp: VecDeque::new(),
        }
    }
}

impl TempBuffer{
    fn gettok(&mut self) -> Token {
        if self.temp.is_empty() {
            scan_tok(&mut self.buffer)
        } else {
            self.temp.pop_front().unwrap()
        }
    }

    fn push_front(&mut self,token: Token) {
        self.temp.push_front(token);
    }
}


fn scan_tok(buffer: &mut Buffer) -> Token {
    let mut lastchar = char::default();
    let mut curr_buffer = String::new();
    loop {
        lastchar = buffer.getchar();
        if lastchar == ' ' || lastchar == '\n' || lastchar == '\r' {
            continue;
        }

        if lastchar.is_alphabetic() {
            while lastchar.is_alphanumeric() {
                curr_buffer.push(lastchar);
                lastchar = buffer.getchar();
            }
            buffer.push_front(lastchar);
            match curr_buffer.as_str() {
                "while" => return Token::While,
                "if" => return Token::If,
                "else" => return Token::Else,
                "break" => return Token::Break,
                "continue" => return Token::Continue,
                "return" => return Token::Return,
                "readint" => return Token::ReadInt,
                "writeint" => return Token::WriteInt,
                "int" => return Token::Int,
                "char" => return Token::Char,
                _ => {}
            };
            return Token::Identifier(curr_buffer);
        } else if lastchar.is_digit(10) {
            let mut decimal: bool = false;
            while lastchar.is_digit(10) || lastchar == '.' {
                curr_buffer.push(lastchar);
                lastchar = buffer.getchar();
                if lastchar == '.' {
                    decimal = true;
                }
            }
            buffer.push_front(lastchar);
            if decimal {
                return Token::Float(curr_buffer.parse::<f32>().unwrap());
            } else {
                return Token::Integer(curr_buffer.parse::<i32>().unwrap());
            }
        } else if lastchar == '\"' {
            lastchar = buffer.getchar();
            while lastchar != '\"' {
                curr_buffer.push(lastchar);
                lastchar = buffer.getchar();
            }
            return Token::StringLiteral(curr_buffer);
        } else if lastchar == '\'' {
            lastchar = buffer.getchar();
            curr_buffer.push(lastchar);
            lastchar = buffer.getchar();
            if lastchar == '\'' {
                return Token::Character(curr_buffer.pop().unwrap());
            } else {
                panic!("{}",format!("expected ', found {}",lastchar).as_str());
            }
        } else {
            match lastchar.clone() {
                '=' => {
                    let mut tmp = buffer.getchar();
                    if tmp == '=' {
                        return Token::Ascii("==".into());
                    } else {
                        buffer.push_front(tmp);
                        return Token::Ascii(lastchar.into());
                    }
                },
                '&' => {
                    let mut tmp = buffer.getchar();
                    if tmp == '&' {
                        return Token::Ascii("&&".into());
                    } else {
                        buffer.push_front(tmp);
                        return Token::Ascii(lastchar.into());
                    }
                },
                '|' => {
                    let mut tmp = buffer.getchar();
                    if tmp == '|' {
                        return Token::Ascii("||".into());
                    } else {
                        buffer.push_front(tmp);
                        return Token::Ascii(lastchar.into());
                    }
                },
                _ => {
                    return Token::Ascii(lastchar.into());
                },
            }
        }
    }
}


#[derive(PartialEq,Debug)]
pub enum Primary{
    Num(i32),
    Charconst(char),
    Identifier(String),
    Expr(Box<Expr>),
}

fn parse_primary(tempbuffer: &mut TempBuffer) -> Primary {
    let mut tok = tempbuffer.gettok();
    println!("In parse_primary:{:?}",tok);
    match tok {
        Token::Integer(x) => {
            return Primary::Num(x);
        },
        Token::Character(x) => {
            return Primary::Charconst(x);
        },
        Token::Identifier(x) => {
            return Primary::Identifier(x);
        },
        Token::Ascii(x) if x.as_str() == "(" => {
            let mut tok2 = tempbuffer.gettok();
            match &tok2 {
                Token::Integer(x) => {
                    tempbuffer.push_front(tok2.clone());
                    let expr = parse_expr(tempbuffer);
                    let tok3 = tempbuffer.gettok();
                    match &tok3 {
                        Token::Ascii(x) if x.as_str()== ")" => {
                            return Primary::Expr(Box::new(expr));
                        },
                        _ => panic!("expected ')', found {:?}",tok3),
                    };
                },
                Token::Identifier(x) => {
                    tempbuffer.push_front(tok2.clone());
                    let expr = parse_expr(tempbuffer);
                    let tok3 = tempbuffer.gettok();
                    match &tok3 {
                        Token::Ascii(x) if x.as_str() == ")" => {
                            return Primary::Expr(Box::new(expr));
                        },
                        _ => panic!("expected ')', found {:?}",tok3),
                    };
                },
                Token::Character(x) => {
                    tempbuffer.push_front(tok2.clone());
                    let expr = parse_expr(tempbuffer);
                    let tok3 = tempbuffer.gettok();
                    match &tok3 {
                        Token::Ascii(y) if y.as_str() == ")" => {
                            return Primary::Expr(Box::new(expr));
                        },
                        _ => panic!("expected ')', found {:?}",tok3),
                    };
                },
                Token::Ascii(x) => {
                    if x.as_str() == "!" || x.as_str() == "-" {
                        tempbuffer.push_front(tok2.clone());
                        let expr = parse_expr(tempbuffer);
                        let tok3 = tempbuffer.gettok();
                        match &tok3 {
                            Token::Ascii(y) if y.as_str() == ")" => {
                                return Primary::Expr(Box::new(expr));
                            },
                            _ => panic!("expected ')', found {:?}",tok3),
                        };
                    } else {
                        panic!("expected '!','-', found {:?}",x);
                    }
                },
                x @ _ => panic!("expected Identifier or integer or character or '!' or '-', found {:?}",x),
            }
        },
        x @ _ => {
            panic!("{}",format!("expected integer or character or Identifier or '(', found {:?}",x).as_str());
        },
    }
}

#[derive(PartialEq,Debug)]
pub enum Factor{
    Not(Primary),
    Neg(Primary),
    Primary(Primary),
}

fn parse_factor(tempbuffer: &mut TempBuffer) -> Factor {
    let tok = tempbuffer.gettok();
    println!("In parse_factor {:?}",tok);
    match &tok {
        Token::Ascii(x) if x.as_str() == "!" => {
            let tok2 = tempbuffer.gettok();
            let is_primary: bool = match &tok2 {
                Token::Integer(x) => true,
                Token::Character(x) => true,
                Token::Identifier(x) => true,
                Token::Ascii(x) if x.as_str() == "(" => true,
                _ => false,
            };
            if is_primary {
                tempbuffer.push_front(tok2);
                let primary = parse_primary(tempbuffer);
                return Factor::Not(primary);
            } else {
                panic!("expected integer, character, identifier or '(', found {:?}",tok2); 
            }
        },
        Token::Ascii(x) if x.as_str() == "-" => {
            let tok2 = tempbuffer.gettok();
            let is_primary: bool = match &tok2 {
                Token::Integer(x) => true,
                Token::Character(x) => true,
                Token::Identifier(x) => true,
                Token::Ascii(x) if x.as_str() == "(" => true,
                _ => false,
            };
            if is_primary {
                tempbuffer.push_front(tok2);
                let primary = parse_primary(tempbuffer);
                return Factor::Neg(primary);
            } else {
                panic!("expected integer, character, identifier or '(', found {:?}",tok2);
            }
        },
        x @ _ => {
            let is_primary = match &x {
                Token::Integer(y) => true,
                Token::Character(y) => true,
                Token::Identifier(y) => true,
                Token::Ascii(y) if y.as_str() == "(" => true,
                _ => false,
            };
            if is_primary {
                tempbuffer.push_front(tok.clone());
                let primary = parse_primary(tempbuffer);
                return Factor::Primary(primary);
            } else {
                panic!("expected '!', '-', integer, character, identifier or '(', found {:?}",tok);
            }
        },
    };
}
#[derive(PartialEq,Debug)]
pub struct MultiplyN{
    operand1: Factor,
    operand2: Factor,
}

impl From<(Factor,Factor)> for MultiplyN {
    fn from(f: (Factor,Factor)) -> Self {
        MultiplyN{
            operand1: f.0,
            operand2: f.1,
        }
    }
}

#[derive(PartialEq,Debug)]
pub struct DivisionN{
    operand1: Factor,
    operand2: Factor,
}

impl From<(Factor,Factor)> for DivisionN {
    fn from(f: (Factor,Factor)) -> Self {
        DivisionN{
            operand1: f.0,
            operand2: f.1,
        }
    }
}

#[derive(PartialEq,Debug)]
pub struct ModN{
    operand1: Factor,
    operand2: Factor,
}

impl From<(Factor,Factor)> for ModN{
    fn from(f: (Factor,Factor)) -> Self {
        ModN{
            operand1: f.0,
            operand2: f.1,
        }
    }
}

#[derive(PartialEq,Debug)]
pub enum Term {
    Multiply(MultiplyN),
    Division(DivisionN),
    Mod(ModN),
    Factor(Factor),
}

fn parse_term(tempbuffer: &mut TempBuffer) -> Term {
    let tok = tempbuffer.gettok();
    println!("In parse_term: {:?}",tok);
    let is_factor = match &tok {
        Token::Ascii(x) if x.as_str() == "!" => true,
        Token::Ascii(x) if x.as_str() == "-" => true,
        Token::Integer(x) => true,
        Token::Character(x) => true,
        Token::Identifier(x) => true,
        Token::Ascii(x) if x.as_str() == "(" => true,
        _ => false,
    };
    if is_factor {
        tempbuffer.push_front(tok);
        let factor1 = parse_factor(tempbuffer);
        let tok2 = tempbuffer.gettok();
        match &tok2 {
            Token::Ascii(x) if x.as_str() == "*" => {
                let factor2 = parse_factor(tempbuffer);
                return Term::Multiply((factor1,factor2).into());
            },
            Token::Ascii(x) if x.as_str() == "/" => {
                let factor2 = parse_factor(tempbuffer);
                return Term::Division((factor1,factor2).into());
            },
            Token::Ascii(x) if x.as_str() == "%" => {
                let factor2 = parse_factor(tempbuffer);
                return Term::Mod((factor1,factor2).into());
            },
            _ => {
                tempbuffer.push_front(tok2.clone());
                return Term::Factor(factor1);
            },
        };
    } else {
        panic!("expected '!','-',integer,character, identifier or '(', found {:?}",tok);
    }
}

#[derive(PartialEq,Debug)]
pub struct SubN{
    operand1: Term,
    operand2: Term,
}

impl From<(Term,Term)> for SubN{
    fn from(f: (Term,Term)) -> Self {
        SubN{
            operand1: f.0,
            operand2: f.1,
        }
    }
}

#[derive(PartialEq,Debug)]
pub enum Sum{
    Add(AddN),
    Sub(SubN),
    Term(Term),
}

#[derive(PartialEq,Debug)]
pub struct AddN{
    operand1: Term,
    operand2: Term,
}

impl From<(Term,Term)> for AddN{
    fn from(f: (Term,Term)) -> Self {
        AddN{
            operand1: f.0,
            operand2: f.1,
        }
    }
}

fn parse_sum(tempbuffer: &mut TempBuffer) -> Sum {
    let tok = tempbuffer.gettok();
    println!("In parse_sum: {:?}",tok);
    let is_sum = match &tok {
        Token::Ascii(x) if x.as_str() == "!" => true,
        Token::Ascii(x) if x.as_str() == "-" => true,
        Token::Integer(x) => true,
        Token::Character(x) => true,
        Token::Identifier(x) => true,
        Token::Ascii(x) if x.as_str() == "(" => true,
        _ => false,
    };
    if is_sum {
        tempbuffer.push_front(tok);
        let term1 = parse_term(tempbuffer);
        let tok2 = tempbuffer.gettok();
        match &tok2 {
            Token::Ascii(x) if x.as_str() == "+" => {
                let term2 = parse_term(tempbuffer);
                return Sum::Add((term1,term2).into());
            },
            Token::Ascii(x) if x.as_str() == "-" => {
                let term2 = parse_term(tempbuffer);
                return Sum::Sub((term1,term2).into());
            },
            _ => {
                tempbuffer.push_front(tok2.clone());
                return Sum::Term(term1);
            },
        };
    } else {
        panic!("expected '!', '-', integer, identifier, character or '(', found {:?}",tok);
    }
}

#[derive(PartialEq,Debug)]
pub struct LessThanN{
    operand1: Sum,
    operand2: Sum,
}

impl From<(Sum,Sum)> for LessThanN{
    fn from(f: (Sum,Sum)) -> Self {
        LessThanN{
            operand1: f.0,
            operand2: f.1,
        }
    }
}

#[derive(PartialEq,Debug)]
pub struct GreaterThanN{
    operand1: Sum,
    operand2: Sum,
}

impl From<(Sum,Sum)> for GreaterThanN {
    fn from(f: (Sum,Sum)) -> Self {
        GreaterThanN{
            operand1: f.0,
            operand2: f.1,
        }
    }
}

#[derive(PartialEq,Debug)]
pub enum Relation{
    Sum(Sum),
    LessThan(LessThanN),
    GreaterThan(GreaterThanN),
}

fn parse_relation(tempbuffer: &mut TempBuffer) -> Relation {
    let tok = tempbuffer.gettok();
    println!("In parse_relation {:?}",tok);
    let is_relation = match &tok {
        Token::Ascii(x) if x.as_str() == "!" => true,
        Token::Ascii(x) if x.as_str() == "-" => true,
        Token::Integer(x) => true,
        Token::Character(x) => true,
        Token::Identifier(x) => true,
        Token::Ascii(x) if x.as_str() == "(" => true,
        _ => false,
    };

    if is_relation {
        tempbuffer.push_front(tok.clone());
        let sum1 = parse_sum(tempbuffer);
        let tok2 = tempbuffer.gettok();
        match &tok2 {
            Token::Ascii(x) if x.as_str() == "<" => {
                let sum2 = parse_sum(tempbuffer);
                return Relation::LessThan((sum1,sum2).into());
            },
            Token::Ascii(x) if x.as_str() == ">" => {
                let sum2 = parse_sum(tempbuffer);
                return Relation::GreaterThan((sum1,sum2).into());
            },
            _ => {
                tempbuffer.push_front(tok2.clone());
                return Relation::Sum(sum1);
            },
        };
    } else {
        panic!("expected '!','-',integer,character,identifier,'(', found {:?}",tok);
    }
}

#[derive(PartialEq,Debug)]
pub struct EqualityN{
    relation1: Relation,
    relation2: Relation,
}

impl From<(Relation,Relation)> for EqualityN{
    fn from(f: (Relation,Relation)) -> Self {
        EqualityN{
            relation1: f.0,
            relation2: f.1,
        }
    }
}

#[derive(PartialEq,Debug)]
pub enum Comparison{
    Relation(Relation),
    Equality(EqualityN),
}

fn parse_comparison(tempbuffer: &mut TempBuffer) -> Comparison {
    let tok = tempbuffer.gettok();
    println!("In parse_comparison {:?}",tok);
    let is_comparison = match &tok {
        Token::Identifier(x) => true,
        Token::Ascii(x) if x.as_str() == "!" => true,
        Token::Ascii(x) if x.as_str() == "-" => true,
        Token::Integer(x) => true,
        Token::Ascii(x) if x.as_str() == "(" => true,
        Token::Character(x) => true,
        _ => false,
    };
    if is_comparison {
        tempbuffer.push_front(tok.clone());
        let relation1 = parse_relation(tempbuffer);
        let tok2 = tempbuffer.gettok();
        match &tok2 {
            Token::Ascii(x) if x.as_str() == "==" => {
                let relation2 = parse_relation(tempbuffer);
                return Comparison::Equality((relation1, relation2).into());
            },
            _ => {
                tempbuffer.push_front(tok2.clone());
                return Comparison::Relation(relation1);
            },
        };
    } else {
        panic!("expected identifier, '!', '-', integer, '(', character, found {:?}",tok);
    }
}

#[derive(PartialEq,Debug)]
pub struct LogicalAndN{
    operand1: Comparison,
    operand2: Comparison,
}

impl From<(Comparison,Comparison)> for LogicalAndN {
    fn from(f: (Comparison, Comparison)) -> Self {
        LogicalAndN{
            operand1: f.0,
            operand2: f.1,
        }
    }
}

#[derive(PartialEq,Debug)]
pub enum Conjunction{
    Comparison(Comparison),
    LogicalAnd(LogicalAndN),
}

fn parse_conjunction(tempbuffer: &mut TempBuffer) -> Conjunction {
    let tok = tempbuffer.gettok();
    println!("In parse_conjunction {:?}",tok);
    let is_conjunction = match &tok {
        Token::Identifier(x) => true,
        Token::Ascii(x) if x.as_str() == "!" => true,
        Token::Integer(x) => true,
        Token::Ascii(x) if x.as_str() == "(" => true,
        Token::Character(x) => true,
        Token::Ascii(x) if x.as_str() == "-" => true,
        _ => false,
    };
    if is_conjunction {
        tempbuffer.push_front(tok.clone());
        let comparison1 = parse_comparison(tempbuffer);
        let tok2 = tempbuffer.gettok();
        match &tok2 {
            Token::Ascii(x) if x.as_str() == "&&" => {
                let comparison2 = parse_comparison(tempbuffer);
                return Conjunction::LogicalAnd((comparison1,comparison2).into());
            },
            _ => {
                tempbuffer.push_front(tok2.clone());
                return Conjunction::Comparison(comparison1);
            },
        };
    } else {
        panic!("expected identifier, '!', integer, '(', character or '-', found {:?}",tok);
    }
}

#[derive(PartialEq,Debug)]
pub struct LogicalOrN{
    operand1: Conjunction,
    operand2: Conjunction,
}

impl From<(Conjunction, Conjunction)> for LogicalOrN {
    fn from(f: (Conjunction, Conjunction)) -> Self {
        LogicalOrN{
            operand1: f.0,
            operand2: f.1,
        }
    }
}

#[derive(PartialEq,Debug)]
pub enum Disjunction{
    Conjunction(Conjunction),
    LogicalOr(LogicalOrN),
}

fn parse_disjunction(tempbuffer: &mut TempBuffer) -> Disjunction {
    let tok = tempbuffer.gettok();
    println!("In parse_disjuntion {:?}",tok);
    let is_disjunction = match &tok {
        Token::Identifier(x) => true,
        Token::Ascii(x) if x.as_str() == "!" => true,
        Token::Ascii(x) if x.as_str() == "-" => true,
        Token::Integer(x) => true,
        Token::Ascii(x) if x.as_str() == "(" => true,
        Token::Character(x) => true,
        _ => false,
    };
    
    if is_disjunction {
        tempbuffer.push_front(tok.clone());
        let conjunction1 = parse_conjunction(tempbuffer);
        let tok2 = tempbuffer.gettok();
        match &tok2 {
            Token::Ascii(x) if x.as_str() == "||" => {
                let conjunction2 = parse_conjunction(tempbuffer);
                return Disjunction::LogicalOr((conjunction1, conjunction2).into());
            },
            _ => {
                tempbuffer.push_front(tok2);
                return Disjunction::Conjunction(conjunction1);
            },
        };
    } else {
        panic!("expected identifier, '!', '-', integer, '(' or character, found {:?}",tok);
    }
}

#[derive(PartialEq,Debug)]
pub struct TernaryOperatorN{
    disjunction: Disjunction,
    expr: Box<Expr>,
    condition: Box<Condition>,
}

impl From<(Disjunction, Expr, Condition)> for TernaryOperatorN {
    fn from(f: (Disjunction, Expr, Condition)) -> Self {
        TernaryOperatorN{
            disjunction: f.0,
            expr: Box::new(f.1),
            condition: Box::new(f.2),
        }
    }
}

#[derive(PartialEq,Debug)]
pub enum Condition{
    Disjunction(Disjunction),
    TernaryOperator(Box<TernaryOperatorN>),
}

fn parse_condition(tempbuffer: &mut TempBuffer) -> Condition {
    let tok = tempbuffer.gettok();
    println!("In parse_condition {:?}",tok);
    let is_condition = match &tok {
        Token::Identifier(x) => true,
        Token::Ascii(x) if x.as_str() == "!" => true,
        Token::Ascii(x) if x.as_str() == "-" => true,
        Token::Integer(x) => true,
        Token::Ascii(x) if x.as_str() == "(" => true,
        Token::Character(x) => true,
        _ => false,
    };
    if is_condition {
        tempbuffer.push_front(tok.clone());
        let disjunction = parse_disjunction(tempbuffer);
        let tok2 = tempbuffer.gettok();
        match &tok2 {
            Token::Ascii(x) if x.as_str() == "?" => {
                let expr = parse_expr(tempbuffer);
                let tok3 = tempbuffer.gettok();
                match &tok3 {
                    Token::Ascii(y) if y.as_str() == ":" => {
                        let condition = parse_condition(tempbuffer);
                        return Condition::TernaryOperator(Box::new((disjunction,expr,condition).into()));
                    },
                    _ => {
                        tempbuffer.push_front(tok2.clone());
                        return Condition::Disjunction(disjunction);
                    },
                };
            },
            _ => {
                tempbuffer.push_front(tok2.clone());
                return Condition::Disjunction(disjunction);
            }
        };
    } else {
        panic!("expected identifier, integer, character, '!', '-', or '(', found {:?}",tok);
    }
}

#[derive(PartialEq,Debug)]
pub struct ExprStmt{
    identifier: String,
    expr: Expr,
}

impl From<(String,Expr)> for ExprStmt{
    fn from(f: (String,Expr)) -> Self {
        ExprStmt{
            identifier: f.0,
            expr: f.1,
        }
    }
}

fn parse_expr_statement(tempbuffer: &mut TempBuffer) -> ExprStmt{
    let tok = tempbuffer.gettok();
    println!("In parse_expr_statement {:?}",tok);
    match &tok {
        Token::Identifier(x) => {
            let tok2 = tempbuffer.gettok();
            match &tok2 {
                Token::Ascii(y) if y.as_str() == "=" => {
                    let expr = parse_expr(tempbuffer);
                    let tok3 = tempbuffer.gettok();
                    match &tok3 {
                        Token::Ascii(z) if z.as_str() == ";" => {
                            return (x.clone(),expr).into();
                        },
                        _ => {
                            panic!("expected ';', found {:?}",tok3);
                        },
                    };
                },
                _ => {
                    panic!("expeceted '=', found {:?}", tok2);
                },
            };
        },
        _ => {
            panic!("expected identifier, found {:?}",tok);
        },
    };
}

#[derive(PartialEq,Debug)]
pub struct IdInitN{
    identifier: String,
    expr: Box<Expr>,
}

impl From<(String,Expr)> for IdInitN{
    fn from(f: (String,Expr)) -> Self {
        IdInitN{
            identifier: f.0,
            expr: Box::new(f.1),
        }
    }
}

#[derive(PartialEq,Debug)]
pub enum Expr{
    IdInit(IdInitN),
    Condition(Box<Condition>),
}

fn parse_expr(tempbuffer: &mut TempBuffer) -> Expr {
    let tok = tempbuffer.gettok();
    println!("In parse_expr {:?}",tok);
    match tok.clone() {
        Token::Identifier(x)  => {
            let tok2 = tempbuffer.gettok();
            match tok2.clone() {
                Token::Ascii(y) if x.as_str() == "=" => {
                    let identifier = x;
                    let expr = parse_expr(tempbuffer);
                    return Expr::IdInit((identifier,expr).into());
                },
                _ => {
                    tempbuffer.push_front(tok2);
                    tempbuffer.push_front(tok.clone());
                },
            }
        },
        _ => {
            tempbuffer.push_front(tok.clone());
        },
    }
    let condition = parse_condition(tempbuffer);
    return Expr::Condition(Box::new(condition));
}

#[derive(PartialEq,Debug)]
pub struct WhileStmt{
    expr: Expr,
    stmt: Box<Stmt>,
}

impl From<(Expr,Stmt)> for WhileStmt{
    fn from(f: (Expr,Stmt)) -> Self {
        WhileStmt{
            expr: f.0,
            stmt: Box::new(f.1),
        }
    }
}

fn parse_whilestmt(tempbuffer: &mut TempBuffer) -> WhileStmt {
    let tok = tempbuffer.gettok();
    println!("In parse_whilestmt {:?}",tok);
    match &tok {
        Token::While => {
            let tok2 = tempbuffer.gettok();
            match &tok2 {
                Token::Ascii(x) if x.as_str() == "(" => {
                    let expr = parse_expr(tempbuffer);
                    let tok3 = tempbuffer.gettok();
                    match &tok3 {
                        Token::Ascii(y) if y.as_str() == ")" => {
                            let stmt = parse_stmt(tempbuffer);
                            return (expr,stmt).into();
                        },
                        _ => {
                            panic!("expected ')', found {:?}",tok3);
                        },
                    };
                },
                _ => {
                    panic!("expected '(', found {:?}",tok2);
                },
            };
        },
        _ => {
            panic!("expected keyword 'while', found {:?}",tok);
        },
    };
}

#[derive(PartialEq,Debug)]
pub struct IfStmtN{
    expr: Expr,
    body: Box<Stmt>,
}

impl From<(Expr,Stmt)> for IfStmtN {
    fn from(f: (Expr,Stmt)) -> IfStmtN {
        IfStmtN{
            expr: f.0,
            body: Box::new(f.1),
        }
    }
}

#[derive(PartialEq,Debug)]
pub struct IfElseStmtN{
    expr: Expr,
    true_: Box<Stmt>,
    false_: Stmt,
}

impl From<(Expr,Stmt,Stmt)> for IfElseStmtN {
    fn from(f: (Expr,Stmt,Stmt)) -> Self {
        IfElseStmtN{
            expr: f.0,
            true_: Box::new(f.1),
            false_: f.2,
        }
    }
}

#[derive(PartialEq,Debug)]
pub enum ConditionStmt{
    IfStmt(Box<IfStmtN>),
    IfElseStmt(Box<IfElseStmtN>),
}

fn parse_condition_stmt(tempbuffer: &mut TempBuffer) -> ConditionStmt {
    let tok =tempbuffer.gettok();
    println!("In parse_condition_stmt {:?}",tok);
    match &tok {
        Token::If => {
            let tok2 = tempbuffer.gettok();
            match &tok2 {
                Token::Ascii(x) if x.as_str() == "(" => {
                    let expr = parse_expr(tempbuffer);
                    let tok3 = tempbuffer.gettok();
                    match &tok3 {
                        Token::Ascii(x) if x.as_str() == ")" => {
                            let stmt1 = parse_stmt(tempbuffer);
                            let tok4 = tempbuffer.gettok();
                            match &tok4 {
                                Token::Else => {
                                    let stmt2 = parse_stmt(tempbuffer);
                                    return ConditionStmt::IfElseStmt(Box::new((expr,stmt1,stmt2).into()));
                                },
                                _ => {
                                    tempbuffer.push_front(tok4.clone());
                                    return ConditionStmt::IfStmt(Box::new((expr,stmt1).into()));
                                },
                            };
                        },
                        _ => {
                            panic!("expected ')', found {:?}",tok3);
                        },
                    };
                },
                _ => {
                    panic!("expected '(', found {:?}",tok2);
                },
            };
        },
        _ => {
            panic!("expected keyword 'if', found {:?}",tok);
        },
    };
}

#[derive(PartialEq,Debug)]
pub enum Stmt{
    CompoundStmt(CompoundStmt),
    WhileStmt(Box<WhileStmt>),
    BreakStmt,
    ContinueStmt,
    ReturnStmt(Expr),
    ReadIntStmt(String),
    WriteIntStmt(Expr),
    ConditionStmt(Box<ConditionStmt>),
}

fn parse_stmt(tempbuffer: &mut TempBuffer) -> Stmt {
    let tok = tempbuffer.gettok();
    println!("In parse_stmt {:?}", tok);
    match &tok {
        Token::Ascii(x) if x.as_str() == "{" => {
            tempbuffer.push_front(tok.clone());
            let compound = parse_compound_stmt(tempbuffer);
            return Stmt::CompoundStmt(compound);
        },
        Token::If => {
            tempbuffer.push_front(tok.clone());
            let cond = parse_condition_stmt(tempbuffer);
            return Stmt::ConditionStmt(Box::new(cond));
        },
        Token::While => {
            tempbuffer.push_front(tok.clone());
            let while_stmt = parse_whilestmt(tempbuffer);
            return Stmt::WhileStmt(Box::new(while_stmt));
        },
        Token::Break => {
            let tok2 = tempbuffer.gettok();
            match &tok2 {
                Token::Ascii(x) if x.as_str() == ";" => {
                    return Stmt::BreakStmt;
                },
                _ => {
                    panic!("expected ';', found {:?}", tok.clone());
                },
            };
        },
        Token::Continue => {
            let tok2 = tempbuffer.gettok();
            match &tok2 {
                Token::Ascii(x) if x.as_str() == ";" => {
                    return Stmt::ContinueStmt;
                },
                _ => {
                    panic!("expceted ';', found {:?}",tok2);
                },
            };
        },
        Token::Return => {
            let expr = parse_expr(tempbuffer);
            let tok2 = tempbuffer.gettok();
            match &tok2 {
                Token::Ascii(x) if x.as_str() == ";" => {
                    return Stmt::ReturnStmt(expr);
                },
                _ => {
                    panic!("expected ';', found {:?}",tok2);
                },
            };
        },
        Token::ReadInt => {
            let tok2 = tempbuffer.gettok();
            match &tok2 {
                Token::Ascii(x) if x.as_str() == "(" => {
                    let identifier = tempbuffer.gettok();
                    match &identifier {
                        Token::Identifier(x) => {
                            let tok3 = tempbuffer.gettok();
                            match &tok3 {
                                Token::Ascii(y) if y.as_str() == ")" => {
                                    let tok4 = tempbuffer.gettok();
                                    match &tok4 {
                                        Token::Ascii(z) if z.as_str() == ";" => {
                                            return Stmt::ReadIntStmt(x.clone());
                                        },
                                        _ => {
                                            panic!("expected ';', found {:?}",tok4);
                                        },
                                    };
                                },
                                _ => {
                                    panic!("expected ')', found {:?}",tok3);
                                },
                            };
                        },
                        _ => {
                            panic!("expected identifier, found {:?}",identifier);
                        },
                    };
                },
                _ => {
                    panic!("expected '(', found {:?}", tok2);
                },
            };
        },
        Token::WriteInt => {
            let tok2 = tempbuffer.gettok();
            match &tok2 {
                Token::Ascii(x) if x.as_str() == "(" => {
                    let expr = parse_expr(tempbuffer);
                    let tok4 = tempbuffer.gettok();
                    match &tok4 {
                        Token::Ascii(y) if y.as_str() == ")" => {
                            let tok5 = tempbuffer.gettok();
                            match &tok5 {
                                Token::Ascii(z) if z.as_str() == ";" => {
                                    return Stmt::WriteIntStmt(expr);
                                },
                                _ => {
                                    panic!("expected ';', found {:?}",tok5);
                                },
                            };
                        },
                        _ => {
                            panic!("expected ')', found {:?}",tok4);
                        },
                    };
                },
                _ => {
                    panic!("expected '(', found {:?}",tok2);
                },
            };
        },
        _ => {
            panic!("expected '{{', 'if', 'while','continue', 'break', 'return', 'readint' or 'writeint', found {:?}",tok);
        },
    };
}

#[derive(PartialEq,Debug)]
pub enum VariableId{
    Identifier(String),
    VariableInit(VariableInitN),
}

#[derive(PartialEq,Debug)]
pub struct VariableInitN{
    identifier: String,
    expr: Expr,
}

impl From<(String,Expr)> for VariableInitN {
    fn from(f: (String,Expr)) -> Self {
        VariableInitN{
            identifier: f.0,
            expr: f.1,
        }
    }
}

fn parse_variable_id(tempbuffer: &mut TempBuffer) -> VariableId {
    let tok = tempbuffer.gettok();
    println!("In parse_variable_id {:?}",tok);
    match &tok {
        Token::Identifier(x) => {
            let tok2 = tempbuffer.gettok();
            match &tok2 {
                Token::Ascii(y) if y.as_str() == "=" => {
                    let expr = parse_expr(tempbuffer);
                    return VariableId::VariableInit((x.clone(),expr).into());
                },
                _ => {
                    tempbuffer.push_front(tok2.clone());
                    return VariableId::Identifier(x.clone());
                },
            };
        },
        _ => {
            panic!("expected identifier, found {:?}", tok);
        },
    };
}

#[derive(PartialEq,Debug)]
pub struct VarDeclList {
    var_ids: Vec<VariableId>,
}

impl From<Vec<VariableId>> for VarDeclList {
    fn from(f: Vec<VariableId>) -> Self {
        VarDeclList{
            var_ids: f,
        }
    }
}

fn parse_var_decl_list(tempbuffer: &mut TempBuffer) -> VarDeclList {
    let mut var_ids: Vec<VariableId> = Vec::new();
    loop {
        let tok = tempbuffer.gettok();
        println!("In parse_va_decl_list {:?}",tok);
        match &tok {
            Token::Identifier(x) => {
                tempbuffer.push_front(tok.clone());
                let variable_id = parse_variable_id(tempbuffer);
                var_ids.push(variable_id);
                let tok2 = tempbuffer.gettok();
                match &tok2 {
                    Token::Ascii(y) if y.as_str() == "," => {
                        continue;
                    },
                    _ => {
                        tempbuffer.push_front(tok2.clone());
                        return var_ids.into();
                    },
                };
            },
            _ => {
                panic!("expected identifier, found {:?}",tok);
            },
        };
    }
}

#[derive(PartialEq,Debug)]
pub struct VarDecl{
    type_specifier: TypeSpecifier,
    var_decl_list: VarDeclList,
}

impl From<(TypeSpecifier,VarDeclList)> for VarDecl {
    fn from(f: (TypeSpecifier, VarDeclList)) -> Self {
        VarDecl{
            type_specifier: f.0,
            var_decl_list: f.1,
        }
    }
}

fn parse_var_decl(tempbuffer: &mut TempBuffer) -> VarDecl {
    let type_specifier = parse_type_specifier(tempbuffer);
    let var_decl_list = parse_var_decl_list(tempbuffer);
    let tok = tempbuffer.gettok();
    println!("In parse_var_decl {:?}",tok);
    match &tok {
        Token::Ascii(x) if x.as_str() == ";" => {
            return (type_specifier,var_decl_list).into();
        },
        _ => {
            panic!("expected ';', found {:?}",tok);
        },
    };
}

#[derive(PartialEq,Debug)]
enum CompoundStmts{
    VarDeclsN(VarDecl),
    StmtN(Stmt),
    ExprStmtN(ExprStmt),
}

#[derive(PartialEq,Debug)]
pub struct CompoundStmt {
    compound_stmt_: Vec<CompoundStmts>,
}

impl From<Vec<CompoundStmts>> for CompoundStmt {
    fn from(f: Vec<CompoundStmts>) -> Self {
        CompoundStmt{
            compound_stmt_: f,
        }
    }
}

fn parse_compound_stmt(tempbuffer: &mut TempBuffer) -> CompoundStmt {
    let tok = tempbuffer.gettok();
    println!("In parse_compound_stmt {:?}",tok);
    let mut compound_stmts: Vec<CompoundStmts> = Vec::new();
    match &tok {
        Token::Ascii(x) if x.as_str() == "{" => {
            loop {
                let tok2 = tempbuffer.gettok();
                let is_var_decl = match &tok2 {
                    Token::Int | Token::Char => true,
                    _ => false,
                };
                let is_stmt = match &tok2 {
                    Token::Ascii(x) if x.as_str() == "{" => true,
                    Token::Return => true,
                    Token::If => true,
                    Token::While => true,
                    Token::Break => true,
                    Token::Continue => true,
                    Token::ReadInt => true,
                    Token::WriteInt => true,
                    _ => false,
                };
                let is_end = match &tok2 {
                    Token::Ascii(x) if x.as_str() == "}" => true,
                    _ => false,
                };
                let is_expr_statement = match &tok2 {
                    Token::Identifier(x) => true,
                    _ => false,
                };
                if is_var_decl {
                    tempbuffer.push_front(tok2.clone());
                    let var_decl = parse_var_decl(tempbuffer);
                    compound_stmts.push(CompoundStmts::VarDeclsN(var_decl));
                    continue;
                } else if is_expr_statement {
                    tempbuffer.push_front(tok2.clone());
                    let expr_statement = parse_expr_statement(tempbuffer);
                    compound_stmts.push(CompoundStmts::ExprStmtN(expr_statement));
                    continue;
                } else if is_stmt {
                    tempbuffer.push_front(tok2.clone());
                    let stmt = parse_stmt(tempbuffer);
                    compound_stmts.push(CompoundStmts::StmtN(stmt));
                    continue;
                } else if is_end {
                    return compound_stmts.into();
                } else {
                    panic!("expected '}}', return, if, while, break, continue, readint, writeint, int or char, found {:?}", tok2);
                }
            }
        },
        _ => {
            panic!("expected '{{', found {:?}", tok);
        },
    };
}

#[derive(PartialEq,Debug)]
pub struct ParamDecl{
    type_specifier: TypeSpecifier,
    identifier: String,
}

impl From<(TypeSpecifier, String)> for ParamDecl {
    fn from(f: (TypeSpecifier, String)) -> ParamDecl {
        ParamDecl{
            type_specifier: f.0,
            identifier: f.1,
        }
    }
}

fn parse_param_decl(tempbuffer: &mut TempBuffer) -> ParamDecl {
    let type_specifier = parse_type_specifier(tempbuffer);
    let tok = tempbuffer.gettok();
    println!("In parse_param_decl {:?}",tok);
    match &tok {
        Token::Identifier(x) => {
            return (type_specifier,x.clone()).into();
        },
        _ => {
            panic!("expected identifier, found {:?}",tok.clone());
        },
    };
}

#[derive(PartialEq,Debug)]
pub struct ParamDeclList{
    param_decls: Vec<ParamDecl>,
}

impl From<Vec<ParamDecl>> for ParamDeclList {
    fn from(f: Vec<ParamDecl>) -> ParamDeclList {
        ParamDeclList{
            param_decls: f,
        }
    }
}

fn parse_param_decl_list(tempbuffer: &mut TempBuffer) -> ParamDeclList {
    let mut param_decls: Vec<ParamDecl> = Vec::new();
    loop {
        let param_decl = parse_param_decl(tempbuffer);
        param_decls.push(param_decl);
        let tok = tempbuffer.gettok();
        println!("In parse_param_decl_list {:?}",tok);
        match &tok {
            Token::Ascii(x) if x.as_str() == "," => {
                continue;
            },
            _ => {
                tempbuffer.push_front(tok.clone());
                return param_decls.into();
            },
        };
    }
}

#[derive(PartialEq,Debug)]
pub enum TypeSpecifier{
    Int,
    Char,
}

fn parse_type_specifier(tempbuffer: &mut TempBuffer) -> TypeSpecifier {
    let tok = tempbuffer.gettok();
    println!("In parse_type_specifier {:?}",tok);
    match &tok {
        Token::Int => TypeSpecifier::Int,
        Token::Char => TypeSpecifier::Char,
        _ => {
            panic!("expected int or char, found {:?}",tok);
        },
    }
}

#[derive(PartialEq,Debug)]
pub struct Program{
    type_specifier: TypeSpecifier,
    identifier: String,
    param_decl_list: ParamDeclList,
    compound_stmt: CompoundStmt,
}

impl From<(TypeSpecifier,String,ParamDeclList,CompoundStmt)> for Program {
    fn from(f: (TypeSpecifier,String,ParamDeclList,CompoundStmt)) -> Program {
        Program{
            type_specifier: f.0,
            identifier: f.1,
            param_decl_list: f.2,
            compound_stmt: f.3,
        }
    }
}

fn parse_program(tempbuffer: &mut TempBuffer) -> Program {
    let type_specifier = parse_type_specifier(tempbuffer);
    let tok = tempbuffer.gettok();
    println!("In parse_program {:?}",tok);
    match &tok {
        Token::Identifier(x) => {
            let tok2 = tempbuffer.gettok();
            match &tok2 {
                Token::Ascii(y) if y.as_str() == "(" => {
                    let param_decl_list = parse_param_decl_list(tempbuffer);
                    let tok3 = tempbuffer.gettok();
                    match &tok3 {
                        Token::Ascii(z) if z.as_str() == ")" => {
                            let compound = parse_compound_stmt(tempbuffer);
                            return (type_specifier,x.clone(),param_decl_list,compound).into();
                        },
                        _ => {
                            panic!("expectec ')', found {:?}",tok3);
                        },
                    };
                },
                _ => {
                    panic!("expected '(', found {:?}",tok2);
                },
            };
        },
        _ => {
            panic!("expected identifier, found {:?}",tok);
        },
    };
}

fn main() {
    let mut data: String = String::new();
    std::io::stdout().write_all(b">>").unwrap();
    std::io::stdin().read_line(&mut data).unwrap();
    let mut buff:VecDeque<char> = VecDeque::default();
    data.chars()
        .for_each(|x| {
            buff.push_back(x.clone());
        });
    let mut buffer: Buffer = buff.into();
    let mut tempbuffer: TempBuffer = buffer.into();
    let program = parse_program(&mut tempbuffer);
    println!("{:#?}",program);
}
